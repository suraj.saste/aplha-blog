require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

  def setup
    @category = Category.new
  end

  test "Category should be valid" do
    @category.name = 'Sports'
    assert @category.valid?
  end

  test 'Name should be present' do
    @category.name = ""
    assert_not @category.valid?
  end

  test "Name should be unique" do
    @category.name = 'abc'
    @category.save
    @category2 = Category.new(name: 'abc')
    assert_not @category2.valid?
  end

  test "Length does not matches" do
    @category.name = "aa"
    assert_not @category.valid?
  end
end