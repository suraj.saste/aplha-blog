class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash[:notice] = "Logged in successfully"
      redirect_to user_path(user)
    else
      flash.now[:alert] = 'Wrong details!!'
      render 'new'
    end
  end

  def destroy
    flash[:notice] = "Logged out"
    session[:user_id] = nil
    redirect_to root_path
  end
end
