class CategoriesController < ApplicationController
  before_action :set_category, only: [:edit, :update, :show]
  before_action :authorize_admin, except: [:show, :index]
  
  def index
    @categories = Category.paginate(page: params[:page], per_page: 5)
  end

  def new
    @category = Category.new
  end

  def edit
  end

  def update
    if @category.update(category_params)
      flash[:notice] = 'Category was updated successfully'
      redirect_to category_path(@category)
    else
      render 'edit'
    end
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:notice] = "Category was created successfully"
      redirect_to category_path(@category)
    else
      render 'new'
    end
  end

  def show
    @articles = @category.articles.paginate(page: params[:page], per_page: 5)
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name)
  end

  def authorize_admin
    if !(logged_in? and current_user.admin?)
      flash[:alert] = 'Un-authorize access!!'
      redirect_to categories_path
    end
  end
end
